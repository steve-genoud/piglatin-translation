## Pig Latin for Fun and Profit

This is a small Pig Latin translator exercise. 

To play with the development server you can do:

```
git clone git@gitlab.com:steve-genoud/piglatin-translation.git
npm install
npm start
```


A couple of comments on some choices that were made.

* Most of the UI code lives in one file. The logic of a simple translator is
  not very complex. Having everything in one place actually makes it more
  readable. Obviously, for more complex projects one would separate
  components.

* Only the "business logic" is tested. This is something I strongly believe in.
  I tend to find the return on investment of testing the UI itself usually very
  low. Some end to end tests are useful (to make sure that the app is actually
  working), but this little app would not benefit from it.

* Bootstrap is imported via a CDN (and not built in with webpack). To be
  honest, I have lost some time trying to make it import it – it reminded the
  pain of last time I did it and forgot about it as soon as it was done. As
  I do not use much of Bootstrap (and do not use its mixins), the CDN is good
  enough.


Additionally, for a production project I would:

* Do a build script to tie up the latest details (that I have done by hand
  here, or just ignored): set angular in production mode, uglify the code, make
  sure to bust the cache with different versions of the code, upload it to s3.

* Hooked the system with a CI to run the test automatically (and deploy it
  automatically as well.

* Instrument the whole app (to make sure that real users do not break it).
