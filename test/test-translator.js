import translator from '../src/pig-latin-translator';
import 'should';

// Wikipedia examples. Commented examples do not work with the definition
// specified in the exercice.
var wikipediaExamples = [
  ['pig', 'igpay'],
  ['banana', 'ananabay'],
  //['trash', 'ashtray'],
  ['happy', 'appyhay'],
  ['duck', 'uckday'],
  //['glove', 'oveglay'],
  ['latin', 'atinlay'],
  ['dopest', 'opestday'],
  ['eat', 'atei'],
  ['omelet', 'meletoi'],
  ['are', 'reai'],
  ['apple', 'ppleai'],
];

describe('word translator', () => {

  it('should translate the wikipedia exmpales', () => {
    wikipediaExamples.forEach(word => {
      translator.translateWord(word[0]).should.equal(word[1]);
    });
  });

  it('should manage capitalization for consonanants', () => {
    translator.translateWord('Latin').should.equal('Atinlay');
    translator.translateWord('Are').should.equal('Reai');
  });


  it('should not be able to handle translations', () => {
    translator.translate('💩').should.equal('[untranslatable]');
  });


});

describe('translator', () => {

  it('should translate a sentence', () => {
    translator.translate('My latin is perfect').should.equal('Ymay atinlay sii erfectpay');
  });

  it('should handle basic punctuation', () => {
    translator.translate('haha, I laugh.').should.equal('ahahay, Ii aughlay.');
  });

});


