import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { Component } from '@angular/core';
import { NgModule } from '@angular/core';
import { Pipe } from '@angular/core';
import translator from './pig-latin-translator';

import template from './pig-latin-template.html';
var myStyle = `
  @keyframes MyAnimation {
    0% { opacity: 0; }
    100% { opacity: 1; }
  }
  .my-animation {
    animation: MyAnimation 1s;
  }
`;


@Pipe({ name: 'piglatin' })
class PigLatinPipe {
  transform = function(value) {
    return translator.translate(value);
  }
}

@Component({
  selector: 'pig-latin-app',
  template: template,
  styles: [myStyle]
})
class PigLatinComponent {
  savedValues = []
  saveResult = function (result) {
    if (! result.trim()) { return; }
    this.savedValues.splice(0, 0, result);
    this.savedValues = this.savedValues.slice(0, 10);
  }
}

@NgModule({
  bootstrap: [PigLatinComponent],
  declarations: [
    PigLatinComponent,
    PigLatinPipe,
  ],
  imports: [
    BrowserModule,
    FormsModule,
  ],
})
class MainModule {};

platformBrowserDynamic().bootstrapModule(MainModule);
