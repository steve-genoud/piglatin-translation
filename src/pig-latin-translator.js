const VOWELS = 'aeiou'.split('');
const CONSONANTS = 'qwrtzpsdfghjlkxcvbnmy'.split('');

var isUppercase = function(txt) {
  return txt === txt.toUpperCase();
};

var translateWord = function(word) {
    if (! word.length) return word; // nothing to translate here

    var lowerCaseFirstLetter = word.slice(0, 1).toLowerCase();
    var outWord;
    if (VOWELS.includes(lowerCaseFirstLetter)) {
       outWord = word.slice(1) + lowerCaseFirstLetter + 'i';
    }
     else if (CONSONANTS.includes(lowerCaseFirstLetter)){
       outWord = word.slice(1) + lowerCaseFirstLetter + 'ay';
     } else {
       // Sometimes people try to translate things that did not make sense in
       // the times of the Romans
       return '[untranslatable]';
     }

       // Let's not forget to keep the first letter uppercase if it was so before
       if (isUppercase(word[0])) {
         outWord = outWord[0].toUpperCase() + outWord.slice(1);
       }

       return outWord;
};

var handlePunctuationDecorator = function(translator) {
  return function (word) {

    // We support only a tiny bit of punctuation, romans did as well. Too bad
    // if your sentence contains weird punctuation.
    if (word.endsWith('.') || word.endsWith(',') || word.endsWith('!') || word.endsWith('?')) {
      return translator(word.slice(0, -1)) + word.slice(-1);
    }

    return translator(word);
  };
};

var translate = function (sentence) {
  var words = sentence.split(/\s/);

  // Romans did not not any other separators than the single white space.
  return words.map(handlePunctuationDecorator(translateWord)).join(' ');
};

export default {
  translate: translate,
  translateWord: translateWord,
};
